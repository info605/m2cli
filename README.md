cpscli
======



[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/cpscli.svg)](https://npmjs.org/package/cpscli)
[![Downloads/week](https://img.shields.io/npm/dw/cpscli.svg)](https://npmjs.org/package/cpscli)
[![License](https://img.shields.io/npm/l/cpscli.svg)](https://github.com/D:/cpscli/blob/master/package.json)

<!-- toc -->
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->
# Usage
<!-- usage -->
```sh-session
$ npm install -g cpscli
$ cpscli COMMAND
running command...
$ cpscli (-v|--version|version)
cpscli/0.0.0 win32-x64 node-v12.14.0
$ cpscli --help [COMMAND]
USAGE
  $ cpscli COMMAND
...
```
<!-- usagestop -->
# Commands
<!-- commands -->
* [`cpscli clear`](#cpscli-clear)
* [`cpscli help [COMMAND]`](#cpscli-help-command)
* [`cpscli init`](#cpscli-init)
* [`cpscli install`](#cpscli-install)
* [`cpscli workspace:create`](#cpscli-workspacecreate)
* [`cpscli workspace:list`](#cpscli-workspacelist)
* [`cpscli workspace:reload NAME`](#cpscli-workspacereload-name)
* [`cpscli workspace:remove NAME`](#cpscli-workspaceremove-name)
* [`cpscli workspace:resume NAME`](#cpscli-workspaceresume-name)
* [`cpscli workspace:run NAME`](#cpscli-workspacerun-name)
* [`cpscli workspace:status NAME`](#cpscli-workspacestatus-name)
* [`cpscli workspace:stop NAME`](#cpscli-workspacestop-name)
* [`cpscli workspace:suspend NAME`](#cpscli-workspacesuspend-name)

## `cpscli clear`

Removes the program folder

```
USAGE
  $ cpscli clear
```

## `cpscli help [COMMAND]`

display help for cpscli

```
USAGE
  $ cpscli help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v2.2.3/src\commands\help.ts)_

## `cpscli init`

Init the program folder

```
USAGE
  $ cpscli init
```

## `cpscli install`

Install all necessary programs

```
USAGE
  $ cpscli install

OPTIONS
  -p, --program=program  program to install

DESCRIPTION
  Install Vagrant and VirtualBox
```

## `cpscli workspace:create`

Creates a workspace

```
USAGE
  $ cpscli workspace:create

OPTIONS
  --boxName=boxName                (required) Name of the vagrant box
  --boxVersion=boxVersion          (required) Version of the vagrant box
  --cpu=cpu                        [default: 4] CPU limit of the virtual machine
  --ip=ip                          [default: 192.168.50.4] IP of the workspace
  --memory=memory                  [default: 4096] RAM Memory limit of the virtual machine
  --name=name                      [default: vagrant_1577422268] Name of the workspace
  --proxyContainer=proxyContainer  [default: certiprosolutions/rproxy] Proxy docker container name
```

## `cpscli workspace:list`

List all workspaces

```
USAGE
  $ cpscli workspace:list
```

## `cpscli workspace:reload NAME`

Reload a workspace

```
USAGE
  $ cpscli workspace:reload NAME
```

## `cpscli workspace:remove NAME`

Removes a workspace

```
USAGE
  $ cpscli workspace:remove NAME
```

## `cpscli workspace:resume NAME`

Resume a workspace

```
USAGE
  $ cpscli workspace:resume NAME
```

## `cpscli workspace:run NAME`

Runs a workspace

```
USAGE
  $ cpscli workspace:run NAME
```

## `cpscli workspace:status NAME`

Get a status of workspace

```
USAGE
  $ cpscli workspace:status NAME
```

## `cpscli workspace:stop NAME`

Stops a workspace

```
USAGE
  $ cpscli workspace:stop NAME
```

## `cpscli workspace:suspend NAME`

Suspend a workspace

```
USAGE
  $ cpscli workspace:suspend NAME
```
<!-- commandsstop -->
