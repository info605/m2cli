const CommandExtend = require('../models/commandextend')
const InitModel = require('../models/init')


class InitCommand extends CommandExtend {
  
  constructor(...args) {
        super(...args)
  }

  async run() {
    try{
      (new InitModel()).run()
    }catch(e) {
      this.error(e)
    }
  }
}

InitCommand.description = `Initialize program`

module.exports = InitCommand