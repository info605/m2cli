const {flags} = require('@oclif/command')
const CommandExtend = require('../models/commandextend')
const ClearModel = require('../models/clear')


class ClearCommand extends CommandExtend {
  
  constructor(...args) {
        super(...args)
  }

  async run() {
    const {flags} = await this.parseWithPrompt(ClearCommand)
    
    try{
      (new ClearModel()).run(flags)
    }catch(e) {
      this.error(e)
    }
  }
}

ClearCommand.description = `Removes the program folder`

ClearCommand.flags = {
  destroy: flags.string({
    description: 'Destroy all workspaces',
    multiple: false,
    required: true,
    choices: [{name: 'Yes'}, {name: 'No'}],
    default: 'No'
  })
}

module.exports = ClearCommand