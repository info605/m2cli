const {flags} = require('@oclif/command')
const BitbucketReposModel = require('../../models/bitbucket/repos')
const CommandExtend = require('../../models/commandextend')
const Global = require('../../helpers/Global')

class BitbucketReposCommand extends CommandExtend {
  
  constructor(...args) {
        super(...args)       
  }
  
  async run() {
    const {flags} = await this.parseWithPrompt(BitbucketReposCommand)

    try{
      var repos = await (new BitbucketReposModel()).run(flags)

      this.log(repos)
      // repos.forEach(element => {
      //   this.log(element.name +'\r')
      // })
    }catch(e) {
      this.error(e)
    }
    
  }
}

BitbucketReposCommand.description = `List all repos for the team`


BitbucketReposCommand.flags = {

  workspace: flags.string({
    description: 'Name of the workspace',
    multiple: false,
    default: `Global`,
    required: true,
    choices: Global.getAvailableWorkspaceList(true)
  }),
  team: flags.string({
    description: 'Name of the team',
    multiple: false,
    required: true,
    choices: Global.getAvailableTeamsList
  }),
}

module.exports = BitbucketReposCommand