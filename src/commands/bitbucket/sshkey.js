
const {flags} = require('@oclif/command')
const BitbucketSSHModel = require('../../models/bitbucket/ssh')
const CommandExtend = require('../../models/commandextend')
const Global = require('../../helpers/Global')

class BitbucketSSHCommand extends CommandExtend {
  
  constructor(...args) {
        super(...args)       
  }
  
  async run() {
    const {flags} = await this.parseWithPrompt(BitbucketSSHCommand)

    try{
       (new BitbucketSSHModel()).run(flags)
    }catch(e) {
      this.error(e)
    }
    
  }
}

BitbucketSSHCommand.description = `Add SSH key to account`

BitbucketSSHCommand.flags = {

  workspace: flags.string({
    description: 'Name of the workspace',
    multiple: false,
    default: `Global`,
    required: true,
    choices: Global.getAvailableWorkspaceList(true)
  })
}

module.exports = BitbucketSSHCommand