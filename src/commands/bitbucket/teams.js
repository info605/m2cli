const {flags} = require('@oclif/command')
const BitbucketTeamsModel = require('../../models/bitbucket/teams')
const CommandExtend = require('../../models/commandextend')
const Global = require('../../helpers/Global')

class BitbucketTeamsCommand extends CommandExtend {
  
  constructor(...args) {
        super(...args)       
  }
  
  async run() {
    const {flags} = await this.parseWithPrompt(BitbucketTeamsCommand)

    try{
       console.log(await (new BitbucketTeamsModel()).run(flags))
    }catch(e) {
      this.error(e)
    }
    
  }
}

BitbucketTeamsCommand.description = `List all teams`


BitbucketTeamsCommand.flags = {
  workspace: flags.string({
    description: 'Name of the workspace',
    multiple: false,
    default: `Global`,
    required: true,
    choices: Global.getAvailableWorkspaceList(true)
  })
}

module.exports = BitbucketTeamsCommand