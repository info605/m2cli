const {flags} = require('@oclif/command')
const BitbucketAuthModel = require('../../models/bitbucket/auth')
const CommandExtend = require('../../models/commandextend')
const Global = require('../../helpers/Global')

class BitbucketAuthCommand extends CommandExtend {
  
  constructor(...args) {
        super(...args)       
  }
  
  async run() {
    const {flags} = await this.parseWithPrompt(BitbucketAuthCommand)

    try{
       (new BitbucketAuthModel()).run(flags)
    }catch(e) {
      this.error(e)
    }
    
  }
}

BitbucketAuthCommand.description = `Auth to Bitbucket`


BitbucketAuthCommand.flags = {

  workspace: flags.string({
    description: 'Name of the workspace',
    multiple: false,
    default: `Global`,
    required: true,
    choices: Global.getAvailableWorkspaceList(true)
  }),
  username: flags.string({
    description: 'Bitbucket username',
    multiple: false,
    required: true
  }),
  password: flags.string({
    description: 'Bitbucket password',
    multiple: false,
    type: 'password',
    required: true
  })
}

module.exports = BitbucketAuthCommand