const {flags} = require('@oclif/command')
const BitbucketBranchesModel = require('../../models/bitbucket/branches')
const CommandExtend = require('../../models/commandextend')
const Global = require('../../helpers/Global')

class BitbucketBranchesCommand extends CommandExtend {
  
  constructor(...args) {
        super(...args)       
  }
  
  async run() {
    const {flags} = await this.parseWithPrompt(BitbucketBranchesCommand)

    try{
      var branches = await (new BitbucketBranchesModel()).run(flags)
      this.log(branches)
    }catch(e) {
      this.error(e)
    }
    
  }
}

BitbucketBranchesCommand.description = `List all branches for the repo`


BitbucketBranchesCommand.flags = {

  workspace: flags.string({
    description: 'Name of the workspace',
    multiple: false,
    default: `Global`,
    required: true,
    choices: Global.getAvailableWorkspaceList(true)
  }),
  team: flags.string({
    description: 'Name of the team',
    multiple: false,
    required: true,
    choices: Global.getAvailableTeamsList
  }),
  repo: flags.string({
    description: 'Name of the repo',
    multiple: false,
    required: true,
    choices: Global.getAvailableRepoList
  }),
}

module.exports = BitbucketBranchesCommand