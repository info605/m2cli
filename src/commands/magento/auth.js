const {flags} = require('@oclif/command')
const MagentoAuthodel = require('../../models/magento/auth')
const CommandExtend = require('../../models/commandextend')
const Global = require('../../helpers/Global')


class MagentoAuthCommand extends CommandExtend {
  
  constructor(...args) {
        super(...args)       
  }
  
  async run() {
    const {flags} = await this.parseWithPrompt(MagentoAuthCommand)

    try{
       (new MagentoAuthodel()).run(flags)
    }catch(e) {
      this.error(e)
    }
    
  }
}

MagentoAuthCommand.description = `Auth to Magento cloud`


MagentoAuthCommand.flags = {

  workspace: flags.string({
    description: 'Name of the workspace',
    multiple: false,
    default: `global`,
    required: true,
    choices: Global.getAvailableWorkspaceList(true)
  }),
  username: flags.string({
    description: 'Magento username',
    multiple: false,
    required: true
  }),
  password: flags.string({
    description: 'Magento password',
    multiple: false,
    type: 'password',
    required: true
  }),
  public_key: flags.string({
    description: 'Magento Access Public Key',
    multiple: false,
    required: true
  }),
  private_key: flags.string({
    description: 'Magento Access Private Key',
    multiple: false,
    required: true
  }),



  
}

module.exports = MagentoAuthCommand