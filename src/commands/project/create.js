const {Command, flags} = require('@oclif/command')
const homedir = require('os').homedir()
const path = require('path')
const fs = require('fs')
const SSHExec = require('ssh-exec')
const WorkSpaceSSH = require('../workspace/ssh')
const extractDomain =  require('extract-domain')
const GitUrlParse = require("git-url-parse")
var hostile = require('hostile')

const Global = require('../../helpers/Global')

const CommandExtend = require('../../models/commandextend')


class ProjectInitCommand extends CommandExtend {
  
  constructor(...args) {
        super(...args)       
  }
  
  async run() {

    const {flags} = await this.parseWithPrompt(ProjectInitCommand)
    
    if (!fs.existsSync(`${homedir}${path.sep}.M2CLI${path.sep}workspaces${path.sep}${flags.workspace}${path.sep}Vagrantfile`)){
        this.error(`Workspace with name ${flags.workspace} doesn't exists. Please run workspace:create to create one`)
    }else {
        var WorkSpace = new WorkSpaceSSH()
        var ssh = WorkSpace.getSSHData(flags.workspace)
        var repoDomain = extractDomain(flags.repo)
        var repoURL = GitUrlParse(flags.repo)
        flags.name = flags.name ? flags.name : repoURL.name 

        var bitbucketFilename = `${homedir}${path.sep}.M2CLI${path.sep}workspaces${path.sep}${flags.workspace}${path.sep}bitbucket.json`
        if(!fs.existsSync(bitbucketFilename)){
          bitbucketFilename = `${homedir}${path.sep}.M2CLI${path.sep}bitbucket.json`
        }

        var magentoFilename = `${homedir}${path.sep}.M2CLI${path.sep}workspaces${path.sep}${flags.workspace}${path.sep}magento.json`
        if(!fs.existsSync(magentoFilename)){
          magentoFilename = `${homedir}${path.sep}.M2CLI${path.sep}magento.json`
        }

        
        if(!fs.existsSync(bitbucketFilename)){
          this.error(`Please log in to bitbucket run command bitbucket:auth`)
        }

        if(!fs.existsSync(magentoFilename)){
          this.error(`Please log in to magento run command magento:auth`)
        }
  

        var bitbucketData =   JSON.parse(fs.readFileSync(bitbucketFilename))
        var magentoData =    JSON.parse(fs.readFileSync(magentoFilename))
        var workSpaceData =    JSON.parse(fs.readFileSync(`${homedir}${path.sep}.M2CLI${path.sep}workspaces${path.sep}${flags.workspace}${path.sep}workspace.json`))
        var ip = workSpaceData.ip ? workSpaceData.ip : '127.0.0.1'
        
        hostile.set(ip, `${flags.url} ssh.${flags.url} mysql.${flags.url} phpmyadmin.${flags.url}`, function (err) {
          if (err) {
            console.error(err)
          } else {
            console.log('set hosts successfully!')
          }
        })

        this.log(`Cloning into '${flags.name}'`)
        SSHExec(`ssh-keyscan -H ${repoDomain} >> ~/.ssh/known_hosts && cd ~/Projects && git clone ${flags.repo} ${flags.name} && cd ${flags.name} && git checkout ${flags.branch}`, {
            user: ssh.user,
            host: ssh.host,
            port: ssh.port,
            key: "~",
            password: 'vagrant'
        }, () => {

          SSHExec(`cd ~/Projects/${flags.name} && cp app/etc/env.php.sample app/etc/env.php && cp .env.sample .env && cp global.env.sample global.env`, {
            user: ssh.user,
            host: ssh.host,
            port: ssh.port,
            key: "~",
            password: 'vagrant'
          }, () => {


          SSHExec(`
          cd ~/Projects/${flags.name} && sed -i 's/{project_name}.localhost/${flags.url}/g' .env && \
          sed -i 's/GIT_EMAIL=/GIT_EMAIL=${bitbucketData.input.username}/g' global.env && \ 
          sed -i 's/GIT_USERNAME=/GIT_USERNAME=${bitbucketData.nickname}/g' global.env && \
          sed -i 's/COMPOSER_MAGENTO_USERNAME=/COMPOSER_MAGENTO_USERNAME=${magentoData.username}/g' global.env && \ 
          sed -i 's/COMPOSER_MAGENTO_PASSWORD=/COMPOSER_MAGENTO_PASSWORD=${magentoData.password}/g' global.env && \
          docker-compose up -d
          `, {
              user: ssh.user,
              host: ssh.host,
              port: ssh.port,
              key: "~",
              password: 'vagrant'
          }, () => {}).pipe(process.stdout)

          }).pipe(process.stdout)

          
        }).pipe(process.stdout)

    }

  }
}

ProjectInitCommand.description = `Init the project`

ProjectInitCommand.flags = {
  name: flags.string({
    description: 'Name for the project',
    multiple: false,
    required: false,
    default: ''
  }),
  url: flags.string({
    description: 'Local URL of the project',
    multiple: false,
    default: Global.getDefaultProjectURL,
    required: true
  }),
  workspace: flags.string({
    description: 'Name of the workspace',
    multiple: false,
    required: true,
    choices: Global.getAvailableWorkspaceList()
  }),
  team: flags.string({
    description: 'Name of the team',
    multiple: false,
    required: true,
    choices: Global.getAvailableTeamsList
  }),
  repo: flags.string({
    description: 'Name of the repo',
    multiple: false,
    required: true,
    choices: Global.getAvailableRepoList
  }),
  branch: flags.string({
    description: 'Branch for the project',
    multiple: false,
    required: true,
    default: 'master',
    choices: Global.getAvailableBranchList
  })
}

module.exports = ProjectInitCommand