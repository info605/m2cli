const {Command, flags} = require('@oclif/command')
const Global = require('../../helpers/Global')
const CommandExtend = require('../../models/commandextend')
const ProjectInitModel = require('../../models/project/init')

class ProjectInitCommand extends CommandExtend {
  
  constructor(...args) {
        super(...args)       
  }
  
  async run() {

    const {flags} = await this.parseWithPrompt(ProjectInitCommand)
        try{
          (new ProjectInitModel()).run(flags)
        }catch(e) {
          this.error(e)
        }
    }

}

ProjectInitCommand.description = `Init the project`

ProjectInitCommand.flags = {
  name: flags.string({
    description: 'Name for the project',
    multiple: false,
    required: false,
    default: ''
  }),
  url: flags.string({
    description: 'Local URL of the project',
    multiple: false,
    default: Global.getDefaultProjectURL,
    required: true
  }),
  workspace: flags.string({
    description: 'Name of the workspace',
    multiple: false,
    required: true,
    choices: Global.getAvailableWorkspaceList()
  }),
  team: flags.string({
    description: 'Name of the team',
    multiple: false,
    required: true,
    choices: Global.getAvailableTeamsList
  }),
  repo: flags.string({
    description: 'Name of the repo',
    multiple: false,
    required: true,
    choices: Global.getAvailableRepoList
  }),
  branch: flags.string({
    description: 'Branch for the project',
    multiple: false,
    required: true,
    default: 'master',
    choices: Global.getAvailableBranchList
  })
}

module.exports = ProjectInitCommand