const {Command, flags} = require('@oclif/command')
const util = require('util')
const exec = util.promisify(require('child_process').exec)

class InstallCommand extends Command {
  
  constructor(...args) {
        super(...args)
        this.programs = {
            'chocolate': {
                'dependencies' : [],
                'command' : 'Set-ExecutionPolicy Bypass -Scope Process -Force; iwr https://chocolatey.org/install.ps1 -UseBasicParsing | iex',
                'check' : 'choco -v'
            },
            'virtualbox': {
                'dependencies' : [
                    'chocolate'
                ],
                'command' : 'choco install -yf virtualbox --version 6.0.14',
                'check' : 'choco list -le -r virtualbox'
            },
            'vagrant': {
                'dependencies' : [
                    'chocolate',
                    'virtualbox'
                ],
                'command' : 'choco install -yf vagrant --version 2.2.6',
                'check' : 'choco list -le -r vagrant'
            }
        }
        this.installed = []
  }

  async asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array);
    }
  }

  inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
  }

  getProgramsToInstall(){
    const {flags} = this.parse(InstallCommand)
    if(flags.program) {
        
        if(!this.programs[flags.program]) {
            var warnMsg = `\n`; 
            Object.keys(this.programs).forEach(
                function(k) { 
                    warnMsg += (k+"\n");
                }
            )
            this.error(`Flag --program expects to be one of ${warnMsg.replace(/\s+$/g, '')} `)
        }

        return {
            [flags.program] : this.programs[flags.program]
        }
    }
    return this.programs
  }

  async checkProgramInstalled(command) {
    try {
        const { error, stdout, stderr } = await exec(command)

        if(error || stderr) {
            return false
        } else if(stdout.length) {
            return true
        }
    
        return false

    } catch(e) {
        return false
    }
  }

  async runInstallCommand(command) {
    // -WindowStyle Hidden '-noexit',
    var runCommand = `powershell -Command "Start-Process -WindowStyle Hidden  -Wait powershell  -Verb RunAs -ArgumentList  '${command}'"`

    try {
        var install = await exec(runCommand);
    } catch(e) {
        return false
    }
  }
  
  async installProgram(key, data) {
    if(this.inArray(key, this.installed)) {
        return;
    }
    var isInstalled = await this.checkProgramInstalled(data['check'])
    if(isInstalled) {
        return;
    } 
    console.log(`Installing  ${key}`)

    this.asyncForEach(data.dependencies, async (depKey) => {
        await this.installProgram(depKey, this.programs[depKey])        
    })


    await this.runInstallCommand(data['command'])
    this.installed.push(key)
  }
  
  async run() {
    var programs = this.getProgramsToInstall()
    this.asyncForEach(Object.keys(programs), async (key) => {
        await this.installProgram(key, this.programs[key])
    })
    // this.log(`Installing ${JSON.stringify(programs)} `)
  }
}

InstallCommand.description = `Install all necessary programs
Install Vagrant and VirtualBox
`

InstallCommand.flags = {
  program: flags.string({char: 'p', description: 'program to install'}),
}

module.exports = InstallCommand