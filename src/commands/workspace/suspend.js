const {flags} = require('@oclif/command')
const WorkSpaceSuspendModel = require('../../models/workspace/suspend')
const CommandExtend = require('../../models/commandextend')
const Global = require('../../helpers/Global')

class WorkSpaceSuspendCommand extends CommandExtend {

  constructor(...args) {
        super(...args)       
  }
  async run() {
    const {flags} = await this.parseWithPrompt(WorkSpaceSuspendCommand)

    try{
      this.log(await (new WorkSpaceSuspendModel()).run(flags))
    }catch(e) {
      this.error(e)
    }
   
  }
}

WorkSpaceSuspendCommand.description = `Suspend workspace`

WorkSpaceSuspendCommand.flags = {
  name: flags.string({
    description: 'Name of the workspace',
    multiple: false,
    required: true,
    choices: Global.getAvailableWorkspaceList()
  })
}

module.exports = WorkSpaceSuspendCommand