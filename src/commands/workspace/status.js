const {flags} = require('@oclif/command')
const WorkSpaceStatusModel = require('../../models/workspace/status')
const CommandExtend = require('../../models/commandextend')
const Global = require('../../helpers/Global')

class WorkSpaceStatusCommand extends CommandExtend {

  constructor(...args) {
        super(...args)       
  }
  async run() {
    const {flags} = await this.parseWithPrompt(WorkSpaceStatusCommand)

    try{
      this.log(await (new WorkSpaceStatusModel()).run(flags))
    }catch(e) {
      this.error(e)
    }
   
  }
}

WorkSpaceStatusCommand.description = `Status of workspace`

WorkSpaceStatusCommand.flags = {
  name: flags.string({
    description: 'Name of the workspace',
    multiple: false,
    required: true,
    choices: Global.getAvailableWorkspaceList()
  })
}

module.exports = WorkSpaceStatusCommand