const {flags} = require('@oclif/command')
const WorkSpaceSSHModel = require('../../models/workspace/ssh')
const CommandExtend = require('../../models/commandextend')
const Global = require('../../helpers/Global')

class WorkSpaceSSHCommand extends CommandExtend {

  constructor(...args) {
        super(...args)       
  }
  async run() {
    const {flags} = await this.parseWithPrompt(WorkSpaceSSHCommand)

    try{
      var sshData = await (new WorkSpaceSSHModel()).run(flags)
      sshData["command"] = `ssh ${sshData.user}@${sshData.hostname} -p${sshData.port}`
      this.log(sshData)
    }catch(e) {
      this.error(e)
    }
   
  }
}

WorkSpaceSSHCommand.description = `SSH command for workspace`

WorkSpaceSSHCommand.flags = {
  name: flags.string({
    description: 'Name of the workspace',
    multiple: false,
    required: true,
    choices: Global.getAvailableWorkspaceList()
  })
}

module.exports = WorkSpaceSSHCommand