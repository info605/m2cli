const {flags} = require('@oclif/command')
const WorkSpaceProvisionModel = require('../../models/workspace/provision')
const CommandExtend = require('../../models/commandextend')
const Global = require('../../helpers/Global')

class WorkSpaceProvisionCommand extends CommandExtend {

  constructor(...args) {
        super(...args)       
  }
  async run() {
    const {flags} = await this.parseWithPrompt(WorkSpaceProvisionCommand)
    try{
      this.log(await (new WorkSpaceProvisionModel()).run(flags))
    }catch(e) {
      this.error(e)
    }
   
  }
}

WorkSpaceProvisionCommand.description = `Provision workspace`

WorkSpaceProvisionCommand.flags = {
  name: flags.string({
    description: 'Name of the workspace',
    multiple: false,
    required: true,
    choices: Global.getAvailableWorkspaceList()
  })
}

module.exports = WorkSpaceProvisionCommand