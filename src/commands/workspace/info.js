const {flags} = require('@oclif/command')
const WorkSpaceInfoModel = require('../../models/workspace/info')
const CommandExtend = require('../../models/commandextend')
const Global = require('../../helpers/Global')

class WorkSpaceInfoCommand extends CommandExtend {

  constructor(...args) {
        super(...args)       
  }

  async run() {
    const {flags} = await this.parseWithPrompt(WorkSpaceInfoCommand)
    try{
      this.log((new WorkSpaceInfoModel()).run(flags))
    }catch(e) {
      this.error(e)
    }
  }
}

WorkSpaceInfoCommand.description = `Get information about workspace`

WorkSpaceInfoCommand.flags = {
  name: flags.string({
    description: 'Name of the workspace',
    multiple: false,
    required: true,
    choices: Global.getAvailableWorkspaceList()
  })
}

module.exports = WorkSpaceInfoCommand