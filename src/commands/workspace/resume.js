const {flags} = require('@oclif/command')
const WorkSpaceResumeModel = require('../../models/workspace/resume')
const CommandExtend = require('../../models/commandextend')
const Global = require('../../helpers/Global')

class WorkSpaceResumeCommand extends CommandExtend {

  constructor(...args) {
        super(...args)       
  }
  async run() {
    const {flags} = await this.parseWithPrompt(WorkSpaceResumeCommand)

    try{
      this.log(await (new WorkSpaceResumeModel()).run(flags))
    }catch(e) {
      this.error(e)
    }
   
  }
}

WorkSpaceResumeCommand.description = `Resume workspace`

WorkSpaceResumeCommand.flags = {
  name: flags.string({
    description: 'Name of the workspace',
    multiple: false,
    required: true,
    choices: Global.getAvailableWorkspaceList()
  })
}

module.exports = WorkSpaceResumeCommand