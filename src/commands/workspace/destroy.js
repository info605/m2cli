const {flags} = require('@oclif/command')
const WorkSpaceDestroyModel = require('../../models/workspace/destroy')
const CommandExtend = require('../../models/commandextend')
const Global = require('../../helpers/Global')

class WorkSpaceDestroyCommand extends CommandExtend {

  constructor(...args) {
        super(...args)       
  }
  async run() {
    const {flags} = await this.parseWithPrompt(WorkSpaceDestroyCommand)

    try{
      this.log(await (new WorkSpaceDestroyModel()).run(flags))
    }catch(e) {
      this.error(e)
    }
   
  }
}

WorkSpaceDestroyCommand.description = `Destroy workspace`

WorkSpaceDestroyCommand.flags = {
  name: flags.string({
    description: 'Name of the workspace',
    multiple: false,
    required: true,
    choices: Global.getAvailableWorkspaceList()
  })
}

module.exports = WorkSpaceDestroyCommand