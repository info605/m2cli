const {flags} = require('@oclif/command')
const WorkSpaceListModel = require('../../models/workspace/list')
const CommandExtend = require('../../models/commandextend')

class WorkSpaceListCommand extends CommandExtend {

  constructor(...args) {
        super(...args)       
  }

  async run() {
    const {flags} = await this.parseWithPrompt(WorkSpaceListCommand)
    try{
      this.log((new WorkSpaceListModel()).run(flags))
    }catch(e) {
      this.error(e)
    }
  }
}

WorkSpaceListCommand.description = `Get list of workspaces`

WorkSpaceListCommand.flags = {
 
}

module.exports = WorkSpaceListCommand