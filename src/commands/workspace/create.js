const {flags} = require('@oclif/command')
const WorkSpaceCreateModel = require('../../models/workspace/create')
const CommandExtend = require('../../models/commandextend')

class WorkSpaceCreateCommand extends CommandExtend {
  
  constructor(...args) {
        super(...args)       
  }
  
  async run() {
    const {flags} = await this.parseWithPrompt(WorkSpaceCreateCommand)

    try{
      (new WorkSpaceCreateModel()).run(flags)
    }catch(e) {
      this.error(e)
    }
    
  }
}

WorkSpaceCreateCommand.description = `Creates workspace`

WorkSpaceCreateCommand.flags = {

  name: flags.string({
    description: 'Name of the workspace',
    multiple: false,
    default: `cpscli_default_${Date.now()}`,
    required: true
  }),

  boxName: flags.string({
    description: 'Name of the vagrant box',
    multiple: false,
    required: true,
    default: 'certiprosolutions/ubuntu-docker'
  }),

  boxVersion: flags.string({
    description: 'Version of the vagrant box',
    multiple: false,
    required: true,
    default: '2.0.0'
  }),

  ip: flags.string({
    description: 'IP of the workspace',
    multiple: false,
    required: false,
    default: false
  }),

  memory: flags.string({
    description: 'RAM Memory limit of the virtual machine',
    multiple: false,
    required: false,
    default: '4096'
  }),

  cpu: flags.string({
    description: 'CPU limit of the virtual machine',
    multiple: false,
    required: false,
    default: '4'
  }),

  proxyContainer: flags.string({
    description: 'Proxy docker container name',
    multiple: false,
    required: false,
    default: 'certiprosolutions/rproxy'
  }),

  sshKey: flags.string({
    description: 'Create new ssh key for workspace',
    multiple: false,
    required: false,
    choices: [{name: 'Yes'}, {name: 'No'}],
    default: 'No'
  })

}

module.exports = WorkSpaceCreateCommand