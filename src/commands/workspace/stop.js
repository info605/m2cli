const {flags} = require('@oclif/command')
const WorkSpaceStopModel = require('../../models/workspace/stop')
const CommandExtend = require('../../models/commandextend')
const Global = require('../../helpers/Global')

class WorkSpaceStopCommand extends CommandExtend {

  constructor(...args) {
        super(...args)       
  }
  async run() {
    const {flags} = await this.parseWithPrompt(WorkSpaceStopCommand)

    try{
      this.log(await (new WorkSpaceStopModel()).run(flags))
    }catch(e) {
      this.error(e)
    }
   
  }
}

WorkSpaceStopCommand.description = `Stop workspace`

WorkSpaceStopCommand.flags = {
  name: flags.string({
    description: 'Name of the workspace',
    multiple: false,
    required: true,
    choices: Global.getAvailableWorkspaceList()
  })
}

module.exports = WorkSpaceStopCommand