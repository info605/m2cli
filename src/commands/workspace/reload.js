const {flags} = require('@oclif/command')
const WorkSpaceReloadModel = require('../../models/workspace/reload')
const CommandExtend = require('../../models/commandextend')
const Global = require('../../helpers/Global')


class WorkSpaceReloadCommand extends CommandExtend {

  constructor(...args) {
        super(...args)       
  }
  async run() {
    const {flags} = await this.parseWithPrompt(WorkSpaceReloadCommand)

    try{
      this.log(await (new WorkSpaceReloadModel()).run(flags))
    }catch(e) {
      this.error(e)
    }
   
  }
}

WorkSpaceReloadCommand.description = `Reload workspace`

WorkSpaceReloadCommand.flags = {
  name: flags.string({
    description: 'Name of the workspace',
    multiple: false,
    required: true,
    choices: Global.getAvailableWorkspaceList()
  })
}

module.exports = WorkSpaceReloadCommand