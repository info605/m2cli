const {flags} = require('@oclif/command')
const WorkSpaceRunModel = require('../../models/workspace/run')
const CommandExtend = require('../../models/commandextend')
const Global = require('../../helpers/Global')

class WorkSpaceRunCommand extends CommandExtend {

  constructor(...args) {
        super(...args)       
  }
  async run() {
    const {flags} = await this.parseWithPrompt(WorkSpaceRunCommand)

    try{
      this.log(await (new WorkSpaceRunModel()).run(flags))
    }catch(e) {
      this.error(e)
    }
   
  }
}

WorkSpaceRunCommand.description = `Runs workspace`

WorkSpaceRunCommand.flags = {
  name: flags.string({
    description: 'Name of the workspace',
    multiple: false,
    required: true,
    choices: Global.getAvailableWorkspaceList()
  })
}

module.exports = WorkSpaceRunCommand