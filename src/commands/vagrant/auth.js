const {flags} = require('@oclif/command')
const VagrantAuthModel = require('../../models/vagrant/auth')
const CommandExtend = require('../../models/commandextend')

class VagrantAuthCommand extends CommandExtend {
  
  constructor(...args) {
        super(...args)       
  }
  
  async run() {
    const {flags} = await this.parseWithPrompt(VagrantAuthCommand)

    try{
      console.log(await (new VagrantAuthModel()).run(flags))
    }catch(e) {
      this.error(e)
    }
    
  }
}

VagrantAuthCommand.description = `Authenticate to Vagrant cloud`

VagrantAuthCommand.flags = {

  token: flags.string({
    description: 'Token',
    multiple: false,
    required: true
  }),

}

module.exports = VagrantAuthCommand