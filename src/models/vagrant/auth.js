const Global = require('../../helpers/Global')
var Command = require('../../../node_modules/node-vagrant/src/command')
var util = require('util')

class VagrantAuthModel {
  
  async run(flags) {
    var command = Command.buildCommand(['cloud', 'auth', 'login', '-t',  flags.token])
    var runCommand = util.promisify(Command.runCommand)
    var ret = ''
    await runCommand(command).then((out)=> {
      ret = out
    }, (err)=>{
      throw Error(err)
    })
    return ret
  }
}

module.exports = VagrantAuthModel