const Global = require('../helpers/Global')
const WorkSpaceListModel = require('./workspace/list')
const WorkSpaceDestroyModel = require('./workspace/destroy')
const rimraf = require("rimraf")

class ClearModel {
  
  async run(flags) {
    
    if(flags.destroy === "Yes") {
      var workspaces = await (new WorkSpaceListModel).run()
        for(var workspace in workspaces){
          await (new WorkSpaceDestroyModel).run({
            name: workspace
          })
        }
    }

    for(var path in Global.paths) {
      try{
        rimraf.sync(Global.paths[path])
      }catch(e) {
          throw Error(e)
      }
    }

  }
}

module.exports = ClearModel