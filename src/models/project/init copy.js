const hostile = require('hostile')
const extractDomain =  require('extract-domain')
const slugify = require('slugify')
const SSHExec = require('ssh-exec')
const WorkSpaceInfoModel = require('../workspace/info')
const WorkSpaceSSHModel = require('../workspace/ssh')
const RepoModel = require('../bitbucket/repos')
const BitBucketAuthModel = require('../bitbucket/auth')
const MagentoAuthModel = require('../magento/auth')

class ProjectInitModel {
  
  async run(flags) {

    var workSpaceInfo = await (new WorkSpaceInfoModel()).run({
      name: flags.workspace
    })
    var workSpaceSSH = await (new WorkSpaceSSHModel()).run({
      name: flags.workspace
    })

    var repo = await (new RepoModel()).getRepo({
      workspace: flags.workspace,
      team: flags.team,
      repo: flags.repo
    })

    var bitbucketAuth = await (new BitBucketAuthModel()).getAuthData({
      workspace: flags.workspace
    })

    var magentoAuth = await (new MagentoAuthModel()).getAuthData({
      workspace: flags.workspace
    })


    var repoCloneURL = repo.links.clone[1].href

    var ip = workSpaceInfo.ip ? workSpaceInfo.ip : '127.0.0.1'

    hostile.set(ip, `${flags.url} ssh.${flags.url} xdebug.${flags.url} mysql.${flags.url} phpmyadmin.${flags.url}`, function (err) {
    })

    var repoDomain = extractDomain(repoCloneURL)
    var folderName = slugify(flags.name).toLowerCase()

    SSHExec(`ssh-keyscan -H ${repoDomain} >> ~/.ssh/known_hosts && cd ~/Projects && git clone --branch=${flags.branch} ${repoCloneURL} ${folderName}`, {
      user: workSpaceSSH.user,
      host: workSpaceSSH.hostname,
      port: workSpaceSSH.port,
      key: "~",
      password: 'vagrant'
  }, (log) => {
    SSHExec(`cd ~/Projects/${folderName} && cp app/etc/env.php.sample app/etc/env.php && cp .env.sample .env && cp global.env.sample global.env`, {
      user: workSpaceSSH.user,
      host: workSpaceSSH.hostname,
      port: workSpaceSSH.port,
      key: "~",
      password: 'vagrant'
    }, (log1) => {
      console.log(log1)

    SSHExec(`
    cd ~/Projects/${folderName} && sed -i 's/{project_name}.localhost/${flags.url}/g' .env && \
    sed -i 's/GIT_EMAIL=/GIT_EMAIL=${bitbucketAuth.input.username}/g' global.env && \ 
    sed -i 's/GIT_USERNAME=/GIT_USERNAME=${bitbucketAuth.nickname}/g' global.env && \
    sed -i 's/COMPOSER_MAGENTO_USERNAME=/COMPOSER_MAGENTO_USERNAME=${magentoAuth.public_key}/g' global.env && \ 
    sed -i 's/COMPOSER_MAGENTO_PASSWORD=/COMPOSER_MAGENTO_PASSWORD=${magentoAuth.private_key}/g' global.env && \
    docker-compose up -d
    `, {
        user: workSpaceSSH.user,
        host: workSpaceSSH.hostname,
        port: workSpaceSSH.port,
        key: "~",
        password: 'vagrant'
    }, () => {}).pipe(process.stdout)

    }).pipe(process.stdout)

    
  }).pipe(process.stdout)

  }
}

module.exports = ProjectInitModel