const hostile = require('hostile')
const extractDomain =  require('extract-domain')
const slugify = require('slugify')
const SSHExec = require('ssh-exec')
const WorkSpaceInfoModel = require('../workspace/info')
const WorkSpaceSSHModel = require('../workspace/ssh')
const RepoModel = require('../bitbucket/repos')

class ProjectInitModel {
  
  async run(flags) {

    var workSpaceInfo = await (new WorkSpaceInfoModel()).run({
      name: flags.workspace
    })
    var workSpaceSSH = await (new WorkSpaceSSHModel()).run({
      name: flags.workspace
    })

    var repo = await (new RepoModel()).getRepo({
      workspace: flags.workspace,
      team: flags.team,
      repo: flags.repo
    })

    var repoCloneURL = repo.links.clone[1].href
    var ip = workSpaceInfo.ip ? workSpaceInfo.ip : '127.0.0.1'
    hostile.set(ip, `${flags.url} ssh.${flags.url} xdebug.${flags.url} mysql.${flags.url} phpmyadmin.${flags.url}`, function (err) {
    })

    var repoDomain = extractDomain(repoCloneURL)
    var folderName = slugify(flags.name).toLowerCase()

    SSHExec(`ssh-keyscan -H ${repoDomain} >> ~/.ssh/known_hosts && cd ~/Projects && git clone --branch=${flags.branch} ${repoCloneURL} ${folderName}`, {
      user: workSpaceSSH.user,
      host: workSpaceSSH.hostname,
      port: workSpaceSSH.port,
      key: "~",
      password: 'vagrant'
  }, (log) => {
      
  }).pipe(process.stdout)

  }
}

module.exports = ProjectInitModel