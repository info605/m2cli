const Global = require('../../helpers/Global')
var Command = require('node-vagrant/src/command')
var util = require('util')

class MagentoAuthModel {
  
  async run(flags) {

    if(flags.workspace == 'Global') {
      var config = Global.config.get()
      config['magento_auth'] = flags
      Global.config.set(config)
    }else {
      let workspaces = Global.config.get('workspaces')
      workspaces = workspaces || {}
      workspaces[flags.workspace]['magento_auth'] = flags
      Global.config.set('workspaces', workspaces)
    }
    
  }

  getAuthData(flags) {

    var magento_auth = Global.config.get('magento_auth');

    if(flags.workspace !== 'Global') {
      let workspaces = Global.config.get('workspaces')
      if(typeof workspaces[flags.workspace]['magento_auth'] !== 'undefined') {
        magento_auth = workspaces[flags.workspace]['magento_auth']
      }
    }

    return magento_auth

  }
}

module.exports = MagentoAuthModel