const Global = require('../../helpers/Global')
const Bitbucket = require('bitbucket')
const bitbucket = new Bitbucket({
  hideNotice: true
})


class BitbucketTeamsModel {
  
  async run(flags) {

    var bitbucket_auth = Global.config.get('bitbucket_auth');

    if(flags.workspace !== 'Global') {
      let workspaces = Global.config.get('workspaces')
      if(typeof workspaces[flags.workspace]['bitbucket_auth'] !== 'undefined') {
        bitbucket_auth = workspaces[flags.workspace]['bitbucket_auth']
      }
    }

    bitbucket.authenticate({
        type: 'basic',
        username: bitbucket_auth.input.username,
        password: bitbucket_auth.input.password 
    })


    let { data, headers } = await bitbucket.repositories.list({ username: flags.team, role: 'member', pagelen:100 })

    return data.values
  }

  async getRepo(flags){
    var bitbucket_auth = Global.config.get('bitbucket_auth');

    if(flags.workspace !== 'Global') {
      let workspaces = Global.config.get('workspaces')
      if(typeof workspaces[flags.workspace]['bitbucket_auth'] !== 'undefined') {
        bitbucket_auth = workspaces[flags.workspace]['bitbucket_auth']
      }
    }

    bitbucket.authenticate({
        type: 'basic',
        username: bitbucket_auth.input.username,
        password: bitbucket_auth.input.password 
    })


    let { data, headers } = await bitbucket.repositories.get({ repo_slug: flags.repo,  username: flags.team, role: 'member', pagelen:100 })

    return data
  }
}

module.exports = BitbucketTeamsModel