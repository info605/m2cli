const Global = require('../../helpers/Global')
const Bitbucket = require('bitbucket')
const fs = require('fs')
const bitbucket = new Bitbucket({
  hideNotice: true
})


class BitbucketAuthModel {
  
  async run(flags) {

    var bitbucket_auth = Global.config.get('bitbucket_auth');

    if(flags.workspace !== 'Global') {
      let workspaces = Global.config.get('workspaces')
      if(typeof workspaces[flags.workspace]['bitbucket_auth'] !== 'undefined') {
        bitbucket_auth = workspaces[flags.workspace]['bitbucket_auth']
      }
    }

    bitbucket.authenticate({
        type: 'basic',
        username: bitbucket_auth.input.username,
        password: bitbucket_auth.input.password 
    })

    if(flags.workspace !== "Global") {
        var sshKey = Global.config.get('workspaces')[flags.workspace]['sshKey']
    }else {
        var sshKey = Global.generateSSHKeys()
    }

    bitbucket.users.createSshKey({
      username: bitbucket_auth.username,
      "_body" : {
          key: fs.readFileSync(`${sshKey}.pub`, "utf8"),
          label: flags.workspace ? `M2CLI ${flags.workspace}` : "M2CLI"
      }
    })

    
  }
}

module.exports = BitbucketAuthModel