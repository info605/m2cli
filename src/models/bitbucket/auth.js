const Global = require('../../helpers/Global')

const Bitbucket = require('bitbucket')
const bitbucket = new Bitbucket({
  hideNotice: true
})


class BitbucketAuthModel {
  
  async run(flags) {

    bitbucket.authenticate({
      type: 'basic',
      username: flags.username,
      password: flags.password
    })

    let { data, headers } = await bitbucket.user.get({ })
    data.input = flags

    if(flags.workspace == 'Global') {
      var config = Global.config.get()
      config['bitbucket_auth'] = data
      Global.config.set(config)
    }else {
      let workspaces = Global.config.get('workspaces')
      workspaces = workspaces || {}
      workspaces[flags.workspace]['bitbucket_auth'] = data
      Global.config.set('workspaces', workspaces)
    }
  }

  async getAuthData(flags) {

    var bitbucket_auth = Global.config.get('bitbucket_auth');

    if(flags.workspace !== 'Global') {
      let workspaces = Global.config.get('workspaces')
      if(typeof workspaces[flags.workspace]['bitbucket_auth'] !== 'undefined') {
        bitbucket_auth = workspaces[flags.workspace]['bitbucket_auth']
      }
    }

    return bitbucket_auth

  }
}

module.exports = BitbucketAuthModel