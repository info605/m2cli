const Global = require('../../helpers/Global')
const Bitbucket = require('bitbucket')
const bitbucket = new Bitbucket({
  hideNotice: true
})


class BitbucketTeamsModel {
  
  async run(flags) {

    var bitbucket_auth = Global.config.get('bitbucket_auth');


    if(flags.workspace !== 'Global') {
      let workspaces = Global.config.get('workspaces')
      if(typeof workspaces[flags.workspace]['bitbucket_auth'] !== 'undefined') {
        bitbucket_auth = workspaces[flags.workspace]['bitbucket_auth']
      }
    }

    bitbucket.authenticate({
        type: 'basic',
        username: bitbucket_auth.input.username,
        password: bitbucket_auth.input.password 
    })


    let { data, headers } = await bitbucket.teams.list({ username: bitbucket_auth.username, role: 'member' })

    return data.values
  }
}

module.exports = BitbucketTeamsModel