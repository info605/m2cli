const Global = require('../helpers/Global')

class InitModel {
  
  async run() {
    for(var path in Global.paths){
        Global.createIfNotExist(Global.paths[path])
        Global.generateSSHKeys()
      }
  }
}

module.exports = InitModel