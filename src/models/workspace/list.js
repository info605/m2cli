const Global = require('../../helpers/Global')

class WorkSpaceListModel {
  
  run() {
      return Global.config.get('workspaces')
  }
}

module.exports = WorkSpaceListModel