const Global = require('../../helpers/Global')
const fs = require('fs')

class WorkSpaceCreateModel {
  
  run(flags) {

    if(flags.sshKey && flags.sshKey === "Yes") {
        flags.sshKey = Global.generateSSHKeys(flags.name)
    }else {
        flags.sshKey = Global.generateSSHKeys()
    }

    var uartFile = `${Global.getWorkspaceDir(flags.name)}ubuntu-bionic-18.04-cloudimg-console.log`
    uartFile = uartFile.replace(/\\/g,"\\\\")

    var networkConfig = flags.ip === false ? `
        config.vm.network "forwarded_port", guest: 80,    host: 80
        config.vm.network "forwarded_port", guest: 443,    host: 443
        config.vm.network "forwarded_port", guest: 3306,    host: 3306
        config.vm.network "forwarded_port", guest: 2020,    host: 2020
        config.ssh.password = "vagrant"
        config.ssh.username = "vagrant"
    ` : `
        config.vm.network "private_network", ip: "${flags.ip}"
        #config.vm.network :forwarded_port, guest: 22, host: 2222, id: "ssh", disabled: true
        config.ssh.password = "vagrant"
        config.ssh.username = "vagrant"
        #config.ssh.host = "${flags.ip}"
        #config.ssh.port = "22"
    `
    var vagrantFileTemplate = `
        Vagrant.configure("2") do |config|
        config.vm.box = "${flags.boxName}"
        config.vm.box_version = "${flags.boxVersion}"
        ${networkConfig}
      
        config.vm.provider "virtualbox" do |v|
          v.gui = false
          v.name = "${flags.name}"
          v.customize ["modifyvm", :id, "--memory",              "${flags.memory}"]
          v.customize ["modifyvm", :id, "--cpus",                   "${flags.cpu}"]
          v.customize ["modifyvm", :id, "--cpuexecutioncap",      "100"]
          v.customize [ "modifyvm", :id, "--uart1", "0x3F8", "4" ]
          v.customize [ "modifyvm", :id, "--uartmode1", "file", "${uartFile}" ]
        end
      
        config.vm.provision "shell" do |s|
          ssh_prv_key = ""
          ssh_pub_key = ""
          ssh_host_pub_key = ""
          if File.file?("${flags.sshKey.replace(/\\/g, "\\\\")}")
            ssh_prv_key = File.read("${flags.sshKey.replace(/\\/g, "\\\\")}")
            ssh_pub_key = File.readlines("${flags.sshKey.replace(/\\/g, "\\\\")}.pub").first.strip
          else
            puts "No SSH key found. You will need to remedy this before pushing to the repository."
          end
          if File.file?("#{Dir.home}/.ssh/id_rsa.pub")
            ssh_host_pub_key = File.readlines("#{Dir.home}/.ssh/id_rsa.pub").first.strip
          end
          s.inline = <<-SHELL
            if grep -sq "#{ssh_host_pub_key}" /home/vagrant/.ssh/authorized_keys; then
              echo "SSH keys already provisioned."
              exit 0;
            fi
            echo "SSH key provisioning."
            mkdir -p /home/vagrant/.ssh/
            touch /home/vagrant/.ssh/authorized_keys
            echo #{ssh_host_pub_key} >> /home/vagrant/.ssh/authorized_keys
            echo #{ssh_pub_key} > /home/vagrant/.ssh/id_rsa.pub
            chmod 644 /home/vagrant/.ssh/id_rsa.pub
            echo "#{ssh_prv_key}" > /home/vagrant/.ssh/id_rsa
            chmod 600 /home/vagrant/.ssh/id_rsa
            chown -R vagrant:vagrant /home/vagrant
            exit 0
          SHELL
        end
        config.vm.provision :shell, privileged: false, path: "provision.sh"
      end`;

      var provisionTemplate = `
      #!/bin/bash
          sudo service docker restart
          [ ! -z "$(docker ps -q)" ] &&  docker stop $(docker ps -q)
          [ ! -z "$(docker ps -aq)" ] &&  docker rm -f $(docker ps -aq)
          sudo docker system prune  -f
          sudo service docker restart
          sudo docker run --restart unless-stopped -dt -p 80:80 -p 443:443 -p 3306:3306 -p 2020:22 -v /var/run/docker.sock:/var/run/docker.sock:ro ${flags.proxyContainer}
          mkdir -p /home/vagrant/Projects
          echo "Provisioned"
      `;

      if (!fs.existsSync(Global.getWorkspaceDir(flags.name))){
        Global.createIfNotExist(Global.getWorkspaceDir(flags.name))
        Global.createIfNotExist(`${Global.getWorkspaceDir(flags.name)}Vagrantfile`, vagrantFileTemplate)
        Global.createIfNotExist(`${Global.getWorkspaceDir(flags.name)}provision.sh`, provisionTemplate)
        let workspaces = Global.config.get('workspaces')
        workspaces = workspaces || {}
        workspaces[flags.name] = flags
        Global.config.set('workspaces', workspaces)
      }else {
          throw new Error(`Workspace ${flags.name} exists. Run workspace:remove ${flags.name} to remove it`)
      }

  }
}

module.exports = WorkSpaceCreateModel