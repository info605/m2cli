const Global = require('../../helpers/Global')
const fs = require('fs')


class WorkSpaceInfoModel {
  
  
  run(flags) {

    if(!fs.existsSync(`${Global.getWorkspaceDir(flags.name)}Vagrantfile`)) {
      throw new Error(`Workspace with name ${flags.name} doesn't exists. Please run workspace:create to create one`)
    }else {
      return Global.config.get('workspaces')[flags.name]
    }
   
  }
}

module.exports = WorkSpaceInfoModel