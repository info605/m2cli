const Global = require('../../helpers/Global')
const vagrant = require('node-vagrant')
const fs = require('fs')
vagrant.promisify()


class WorkSpaceSSHModel {
  
  async run(flags) {

    if(!fs.existsSync(`${Global.getWorkspaceDir(flags.name)}Vagrantfile`)) {
      throw new Error(`Workspace with name ${flags.name} doesn't exists. Please run workspace:create to create one`)
    }else {
      var env = {...process.env, ...{VAGRANT_CWD: Global.getWorkspaceDir(flags.name)}}
      var machine = vagrant.create({ env: env})
      var status = [{}]

      await machine.sshConfig().then((out) => {
        status = out
      }, (err) => {
        throw Error(err)
      })
      
      return status[0]
    }
  }
}

module.exports = WorkSpaceSSHModel