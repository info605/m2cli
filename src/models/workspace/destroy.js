const Global = require('../../helpers/Global')
const vagrant = require('node-vagrant')
const fs = require('fs')
const rimraf = require("rimraf")
vagrant.promisify()

class WorkSpaceDestroyModel {
  
  
  async run(flags) {

    if(!fs.existsSync(`${Global.getWorkspaceDir(flags.name)}Vagrantfile`)) {
      throw new Error(`Workspace with name ${flags.name} doesn't exists. Please run workspace:create to create one`)
    }else {
      var env = {...process.env, ...{VAGRANT_CWD: Global.getWorkspaceDir(flags.name)}}
      var machine = vagrant.create({ env: env})
      var ret = ''
      
      await machine.destroy().then((out) => {
        ret = out
        var workspaces = Global.config.get('workspaces')
        
        delete workspaces[flags.name]
        rimraf.sync(Global.getWorkspaceDir(flags.name))
        Global.config.set('workspaces', workspaces)
      }, (err) => {
        throw Error(err)
      })

      return ret

    }
  }
}

module.exports = WorkSpaceDestroyModel