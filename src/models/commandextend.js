const {Command} = require('@oclif/command')
const Global = require('../helpers/Global')
const Inquirer = require('inquirer')

class CommandExtend extends Command {

    _isFunction(obj){
        return !!(obj && obj.constructor && obj.call && obj.apply);
    }

    async parsePrompt(prompt) {
        var  responses = await Inquirer.prompt(prompt)
        return responses[prompt["name"]];
    }

    
    async parseWithPrompt(options){
        var data = this.parse(Global.setNoRequired(options))
        var prompts = []

        for(var flag in options.flags) {

            if(!data["flags"][flag]) {
                var prompt = {
                    name: flag,
                    message: options.flags[flag]['description'],
                    type: options.flags[flag]['type'],
                    default: this._isFunction(options.flags[flag]['default']) ? await options.flags[flag]['default'](data) : options.flags[flag]['default']
                }

                if(options.flags[flag]['required']) {
                    prompt["validate"] = (data) => {
                        return data.length ? true : 'This is a required field'
                    }
                }

                if(options.flags[flag]['choices']) {
                    prompt["type"] ='list'
                    prompt["choices"] = this._isFunction(options.flags[flag]['choices']) ? await options.flags[flag]['choices'](data) : options.flags[flag]['choices']
                }

                if(flag === 'password') {
                    prompt["type"] ='password'
                }
                
                data.flags[flag] = await this.parsePrompt(prompt)
               
            }
        }


        return data
    }
  
}

module.exports = CommandExtend