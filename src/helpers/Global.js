const homedir = require('os').homedir()
const path = require('path')
const fs = require('fs')
const Conf = require('conf')
const envPaths = require('env-paths')
const projectName = 'm2cli'
const ProjectSuffix = ''
const paths = envPaths(projectName, {
    suffix: ProjectSuffix
})
const crypto = require('crypto')
const sshpk  = require('sshpk')
const clonedeep = require('lodash.clonedeep')
const slugify = require('slugify')


class Global{
    
    static config = new Conf({
        projectSuffix: ProjectSuffix,
        projectName: projectName
    })

    static homeDir = homedir
    static sshKeyPath = `${this.homeDir}${path.sep}.ssh${path.sep}`
    static paths = {...paths, ...{
        workspace: `${paths.data}${path.sep}workspaces`
    }}

    static createIfNotExist(path, data = false) {
        if (!fs.existsSync(path)){
            if(!data) {
                fs.mkdirSync(path, { recursive: true })
            }else {
                fs.writeFileSync(path, data);
            }
        }
    }

    static generateSSHKeys(name = ''){
        name = name.length ? 'id_rsa_'+name : 'id_rsa'
        fs.mkdirSync(Global.sshKeyPath, { recursive: true })
    
        if (!fs.existsSync(`${Global.sshKeyPath}${path.sep}${name}`) && !fs.existsSync(`${Global.sshKeyPath}${path.sep}${name}.pub`)){
            const { privateKey, publicKey } = crypto.generateKeyPairSync('rsa', {
                modulusLength: 4096,
                publicKeyEncoding: {
                  type: 'pkcs1',
                  format: 'pem'
                },
                privateKeyEncoding: {
                  type: 'pkcs8',
                  format: 'pem'
                }
              })
          
              const pemKey = sshpk.parseKey(publicKey, 'pem')
              pemKey.comment = name.length ? `${name}@m2cli` : "m2cli@m2cli"
              const sshRsa = pemKey.toString('ssh')
            
              fs.writeFileSync(`${Global.sshKeyPath}${path.sep}${name}`, privateKey)
              fs.writeFileSync(`${Global.sshKeyPath}${path.sep}${name}.pub`, sshRsa)
        }
        return `${Global.sshKeyPath}${path.sep}${name}`
    }

    static getWorkspaceDir(name){
        return `${Global.paths.workspace}${path.sep}${name}${path.sep}`
    }

    static setNoRequired(options){
        
        var data = clonedeep(options)
        for(var flag in data.flags){
            data.flags[flag].required = false
            data.flags[flag].default = null
        }

        return data
     }

     static getAvailableWorkspaceList(addGlobal = false){
        const WorkSpaceListModel = require('../models/workspace/list')
         var list = []
         var workspaces = (new WorkSpaceListModel()).run()
         for(var workspace in workspaces){
            list.push({
                name: workspace
              })
        }

        if(addGlobal) {
            list.push({
                name: 'Global'
              })
        }
        return list
     }

     static async getAvailableTeamsList(data){
        if(typeof data === 'undefined') {
            return []
        }else {
            const TeamsListModel = require('../models/bitbucket/teams')
            var teamList = new TeamsListModel()
            var list = []
            var teams = await teamList.run({
                workspace: data.flags.workspace
            })

            for(var team in teams){
                list.push({
                    name: teams[team].username
                })
            }

            return list
        }
     }

     static async getAvailableRepoList(data){
        if(typeof data === 'undefined') {
            return []
        }else {
            const RepoListModel = require('../models/bitbucket/repos')
            var repoList = new RepoListModel()
            var list = []
            var repos = await repoList.run({
                workspace: data.flags.workspace,
                team: data.flags.team
            })

            for(var repo in repos){
                list.push({
                    name: repos[repo].slug
                })
            }

            return list
        }
     }

     static async getAvailableBranchList(data){
        if(typeof data === 'undefined') {
            return []
        }else {
            const BranchistModel = require('../models/bitbucket/branches')
            var repoList = new BranchistModel()
            var list = []
            var branches = await repoList.run({
                workspace: data.flags.workspace,
                team: data.flags.team,
                repo: data.flags.repo
            })

            for(var branch in branches){
                list.push({
                    name: branches[branch].name
                })
            }

            return list
        }
     }

     static async getDefaultProjectURL(data){
        if(typeof data === 'undefined' || typeof data.flags.name !== 'string' ) {
            return ''
        }else {
            var name = slugify(data.flags.name)
            name = name.toLowerCase()
            return `${name}.local`
        }
     }
}
  
module.exports = Global